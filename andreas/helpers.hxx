#pragma once
#include "Language.hxx"

struct monthInfo {
    unsigned firstWeekDay;
    unsigned lastDay;
};

vector<pair<unsigned, unsigned>> createAllYearsMonth(unsigned fromYear, unsigned toYear, unsigned fromMonth, unsigned toMonth);
string genWeekDays(const Language &language, const unsigned startDay);
monthInfo getMonthInfo(unsigned year, unsigned monthNo, unsigned dayNo = 1U);
void printHelp();
void printMonth(monthInfo monthInfo, unsigned startDay);

